package net.kastermas.exp2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.os.Handler;
import android.text.Layout;
import android.view.animation.*;
import android.widget.*;

public class ProgramLayout extends Activity {

	final static String LABEL = "ProgramLayout";
	
	public void onCreate (Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    Log.v (LABEL, "onCreate called");
	    
	    ScrollView SV = new ScrollView (this);
	    LinearLayout LL = new LinearLayout (this);
	    LL.setOrientation(LinearLayout.VERTICAL);
	    SV.addView(LL);
	    
	    for (int idx = 0; idx < 100; idx++) {
	    	RelativeLayout RL = new RelativeLayout (this);
	    	TextView txt = new TextView (this);
	    	txt.setText("This is text: " + idx + ".");
	    	LL.addView(txt);
	//    	RL.addView(txt, params);
	    	// TODO: figure out how to pass the parameters
	    }
	    
	    setContentView(SV);
	}
}
