package net.kastermas.exp2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.os.Handler;
import android.text.Layout;
import android.view.animation.*;
import android.widget.*;
import android.widget.RelativeLayout.LayoutParams;

public class TextFlow extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.v ("TextFlow", "onCreate called.");
		
		LinearLayout LL = new LinearLayout (this);
		RelativeLayout RL = new RelativeLayout (this);
		ImageView IV = new ImageView (this);
		TextView TV1 = new TextView (this);
		TextView TV2 = new TextView (this);
		
		LL.addView(RL);
		IV.setImageResource(R.id.imageSand2);
		RL.addView(IV);

		TV1.setText("Text view 1.");
		TV2.setText("Text view 2.");

		RL.addView(IV);
		
		setContentView (LL);
	}
	
}
