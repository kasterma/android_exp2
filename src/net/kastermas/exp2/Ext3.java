package net.kastermas.exp2;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.Layout;
import android.util.Log;
import android.view.animation.*;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

public class Ext3 extends Activity {

	final static String LABEL = "Ext3";
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    Log.v (LABEL, "onCreate called");
	    
	    setContentView(R.layout.splash);
    	ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipperB);
    	flipper.setInAnimation(inFromRightAnimation());
    	flipper.setOutAnimation(outToLeftAnimation());
    	flipper.showNext();
	    
	    Log.v (LABEL, "ContentView set");
	    
	    LinearLayout L2 = (LinearLayout) findViewById (R.id.layout2);
	    
	    
//	    Runnable endSplash = new Runnable() {
//	        @Override
//	        public void run() {
//	        	Log.v (LABEL, "run called");
//	        	ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipperB);
//	        	flipper.setInAnimation(inFromRightAnimation());
//	        	flipper.setOutAnimation(outToLeftAnimation());
//	        	flipper.showNext();     
//
//	        	//setContentView (R.layout.splash2);
//	        }
//	    };
//	    
//	    Runnable nextSplash = new Runnable() {
//	        @Override
//	        public void run() {
//	        	Log.v (LABEL, "run called");
//	        	ViewFlipper flipper = (ViewFlipper) findViewById(R.id.flipperB);
//	    	    flipper.setInAnimation(inFromLeftAnimation());
//	    	    flipper.setOutAnimation(outToRightAnimation());
//	    	    flipper.showPrevious(); 
//
//	        	//setContentView (R.layout.splash2);
//	        }
//	    };
//	    
//	    new Handler().postDelayed(endSplash, 5000L);
//	    new Handler().postDelayed(nextSplash, 9000L);
//	    new Handler().postDelayed(endSplash, 13000L);

	}
	
	
	// http://www.inter-fuser.com/2009/07/android-transistions-slide-in-and-slide.html
	
	private Animation inFromRightAnimation() {

		Animation inFromRight = new TranslateAnimation(
		Animation.RELATIVE_TO_PARENT,  +1.0f, Animation.RELATIVE_TO_PARENT,  0.0f,
		Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
		);
		inFromRight.setDuration(500);
		inFromRight.setInterpolator(new AccelerateInterpolator());
		return inFromRight;
	}
	
	private Animation outToLeftAnimation() {
		Animation outtoLeft = new TranslateAnimation(
		 Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,  -1.0f,
		 Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
		);
		outtoLeft.setDuration(500);
		outtoLeft.setInterpolator(new AccelerateInterpolator());
		return outtoLeft;
	}
	
	private Animation inFromLeftAnimation() {
		Animation inFromLeft = new TranslateAnimation(
		Animation.RELATIVE_TO_PARENT,  -1.0f, Animation.RELATIVE_TO_PARENT,  0.0f,
		Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
		);
		inFromLeft.setDuration(500);
		inFromLeft.setInterpolator(new AccelerateInterpolator());
		return inFromLeft;
	}
	
	private Animation outToRightAnimation() {
		Animation outtoRight = new TranslateAnimation(
		 Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,  +1.0f,
		 Animation.RELATIVE_TO_PARENT,  0.0f, Animation.RELATIVE_TO_PARENT,   0.0f
		);
		outtoRight.setDuration(500);
		outtoRight.setInterpolator(new AccelerateInterpolator());
		return outtoRight;
	}
	
}
